<?php
/*
  ./app/routeur.php
*/


if (isset($_GET['commentaire'])):
  include_once '../app/routeurs/commentaires.php';

  /*
    ROUTE PAR DEFAUT
    PATERN:/
    CTRL: commentairesControleur
    ACTION: index
  */
else:
  include '../app/controleurs/commentairesControleur.php';
  \App\Controleurs\CommentairesControleur\indexAction($connexion);
endif;
