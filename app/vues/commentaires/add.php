<?php
/*
  ./app/vues/commentaires/add.php
  variables disponibles :
     - $commentaireID: INT
     - $pseudo STR
     - $commentaire STR
*/
?>

    <li class="collection-item avatar post" data-id="<?php echo $commentaireID; ?>">
         <i class="material-icons circle green">insert_chart</i>
         <div class="title"><?php echo $pseudo; ?></div>
         <div class="text truncate"><?php echo $commentaire; ?></div>
         <div>
           <a href="#" class="edit">Editer le texte</a> |
           <a href="#" class="delete">Supprimer la publication</a>
         </div>
     </li>
