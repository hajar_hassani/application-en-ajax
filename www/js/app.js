/*
  ./www/js/app.js
*/

/* -----------------------------------------------------------
    L'AJOUT
   -----------------------------------------------------------
*/

$(function(){
  $('#form_commentaires').submit(function(e){
    e.preventDefault();

    $.ajax({
      url: 'commentaire/add',
      data: {
              pseudo: $('#pseudo').val(),
              commentaire: $('#commentaire').val()
            },
      method: 'post'
    })
    .done(function(reponsePHP){
      $('#listeDesPosts').prepend(reponsePHP)
                         .find('li:first')
                         .hide()
                         .slideDown();
      $('#pseudo').val('');
      $('#commentaire').val('');
    })
    .fail(function(){
      alert("Problème durant la transaction !");
    });
  });

/* -----------------------------------------------------------
    SUPRESSION
   -----------------------------------------------------------
*/
  $('#listeDesPosts').on('click','.delete',function(e){
    e.preventDefault();
    $.ajax({
      url: 'commentaire/delete/' + $(this).closest('li').attr('data-id'),
      context: this
    })
    .done(function(reponsePHP){
      if(reponsePHP == 1){
        $(this).closest('li').slideUp(function(){ // on fait un remove dans un callback car le remove ne fonctionne pas après un slideUp
          $(this).remove();
        });
      }
    })
    .fail(function(e){
      console.log(e);
    });
  });

/* -----------------------------------------------------------
    EDITER - PARTIE FORMULAIRE .edit
   -----------------------------------------------------------
*/
  $('#listeDesPosts').on('click','.edit',function(e){
    e.preventDefault();
    $(this).toggleClass('edit validate').text('Valider les modifications');

    let elementTexte = $(this).closest('li').find('.text');
    let contenu = elementTexte.text();
    elementTexte.html('<input type="texte" />')
                .find('input')
                .val(contenu);

  });

  /* -----------------------------------------------------------
      EDITER - PARTIE MODIFICATION EN AJAX .validate
     -----------------------------------------------------------
  */
  $('#listeDesPosts').on('click','.validate',function(e){
    let contenu = $(this).closest('li').find('.text input').val(); // La je vais chercher la valeur de l'input qui est dans le .text qui est dans le parent li
    $.ajax({
      url: 'commentaire/update/' + $(this).closest('li').attr('data-id'),
      data: {
        texte: contenu
      },
      method: 'post',  // J'aurais l'id en $_GET et le texte en $_POST
      context: this
    })
     .done(function(reponsePHP){
       // je vais venir prendre le .text et je vais aller modifier le html
       // puisque dedans il y a pour le moment un input et je vais l'écraser avec la valeur de l'input.
       $(this).closest('li').find('.text').html(contenu);
       $(this).toggleClass('edit validate').text('Editer le texte');
     })
     .fail(function(e){
       console.log(e);
     });
  });
});
